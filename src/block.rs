use serde::{Serialize, Deserialize};
use crate::crypto::hash::{H256, Hashable};
// use crate::crypto::merkle::MerkleTree;
// use crate::transaction::{SignedTransaction,generate_random_transaction};
use crate::transaction::SignedTransaction;
// use rand::random;
// use std::time::SystemTime;
#[derive(Serialize, Deserialize, Debug,Clone,Copy)]
pub struct Header {
	pub parent: H256,
	pub nonce: u32,
    pub difficulty_m: H256,
    pub difficulty: H256,
	pub timestamp: u128,
    pub merkle_root_p: H256,
    pub merkle_root_t: H256,
}
impl Hashable for Header {
    fn hash(&self) -> H256 {
        let serialized = serde_json::to_string(self).unwrap();
        ring::digest::digest(&ring::digest::SHA256, serialized.as_bytes()).into()
    }
}
#[derive(Serialize, Deserialize, Debug,Clone)]
pub struct ContentT {
    pub data: Vec<SignedTransaction>,
}
#[derive(Serialize, Deserialize, Debug,Clone)]
pub struct ContentP {
    pub data: Vec<H256>,
}
#[derive(Serialize, Deserialize, Debug,Clone)]
pub struct BlockP {
    pub header : Header,
    pub content: ContentP,
}
#[derive(Serialize, Deserialize, Debug,Clone)]
pub struct BlockT {
    pub header : Header,
    pub content: ContentT,
}
impl Hashable for BlockP {
    fn hash(&self) -> H256 {
        self.header.hash()
    }
}
impl Hashable for BlockT {
    fn hash(&self) -> H256 {
        self.header.hash()
    }
}

#[cfg(any(test, test_utilities))]
pub mod test {
    use super::*;
    use crate::crypto::hash::H256;

    pub fn generate_random_block(parent: &H256) -> BlockP {
        let key = key_pair::random();
        let transaction = generate_random_transaction(key);
        let fake_root = MerkleTree::new(&vec![transaction]).root(); 
        let header = Header{
            parent : *parent,
            nonce : random::<u32>(),
            difficulty_m : *parent,
            difficulty : *parent,
            // timestamp :  SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis(),
            merkle_root_p : fake_root,
            merkle_root_t : fake_root,
        };
        return BlockP{
            header : header,
            content : ContentP {data : Vec::<SignedTransaction>::new(),},
        }
    }
}
