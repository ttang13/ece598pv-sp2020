use crate::network::server::Handle as ServerHandle;
use crate::crypto::hash::{H256,H160,Hashable};
use ring::signature::KeyPair;
use crate::transaction::{SignedTransaction,Transaction,sign,Sign};
use crate::crypto::key_pair;
use crate::network::message::Message;
use crate::blockchain::Blockchain;
use crate::block::BlockT;
use log::{debug,info};
use std::collections::HashMap;

use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time;
use rand::random;
use std::sync::{Arc, Mutex};
use std::thread;

enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    Print,
    Balance,
    Paused,
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    mempool: Arc<Mutex<HashMap<H256 ,SignedTransaction>>>,
    txpool: Arc<Mutex<HashMap<H256 ,BlockT>>>,
    glob_state: Arc<Mutex<HashMap<H256, HashMap<(H256,u32), (u32, H160)> >>>,
    table: Arc<Mutex<Vec<H160>>>,
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the generator thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool: &Arc<Mutex<HashMap<H256 ,SignedTransaction>>>,
    txpool: &Arc<Mutex<HashMap<H256 ,BlockT>>>,
    glob_state: &Arc<Mutex<HashMap<H256, HashMap<(H256,u32), (u32, H160)> >>>,
    table: &Arc<Mutex<Vec<H160>>>,
) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        server: server.clone(),
        blockchain: Arc::clone(blockchain),
        mempool: Arc::clone(mempool),
        txpool: Arc::clone(txpool),
        glob_state: Arc::clone(glob_state),
        table: Arc::clone(table),
    };
    
    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
    }
    pub fn print(&self) {
        self.control_chan.send(ControlSignal::Print).unwrap();
    }
    pub fn pause(&self) {
        self.control_chan.send(ControlSignal::Paused).unwrap();
    }
    pub fn balance(&self) {
        self.control_chan.send(ControlSignal::Balance).unwrap();
    }
    pub fn start(&self, lambda: u64) {
        self.control_chan
            .send(ControlSignal::Start(lambda))
            .unwrap();
    }

}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("generator".to_string())
            .spawn(move || {
                self.generator_loop();
            })
            .unwrap();
        info!("generator initialized into paused mode");
    }

    fn handle_control_signal(&mut self, signal: ControlSignal, addr: H160) {
        match signal {
            ControlSignal::Exit => {
                info!("Generator shutting down");
                self.operating_state = OperatingState::ShutDown;
            }
            ControlSignal::Balance => {
                let tips = self.blockchain.lock().unwrap().tip();
                let coins = self.glob_state.lock().unwrap()[&tips].clone();
                let mut all_my = 0;
                info!("Longest Chain tip is {:?} Length: {}",tips,self.blockchain.lock().unwrap().heights[&tips]);
                info!("tip state {:?}",coins);
                for (_k,(v_a,v_b)) in coins{
                    if v_b == addr{
                        debug!("{} My money",v_a);
                        all_my = all_my + v_a;
                    }
                    else{
                        debug!("{} {:?}'s money",v_a,v_b);
                    }
                }
                info!("All My money {:?}",all_my);
            }
            ControlSignal::Print => {
                let m = self.mempool.lock().unwrap();
                println!("Mempool size: {:?}",m.len());
                for (_,item) in m.clone(){
                    println!("Transactions: {:?}",item.transaction);
                }
                let tx = self.txpool.lock().unwrap();
                println!("Mempool size: {:?}",tx.len());
                for (hash,_item) in tx.clone(){
                    println!("Transactions: {:?}",hash.clone());
                }
                // println!("Mempool: {:?}",self.mempool.lock().unwrap());
            }
            ControlSignal::Start(i) => {
                info!("Generator starting in continuous mode with size {}", i);
                self.operating_state = OperatingState::Run(i);
            }
            ControlSignal::Paused => {
                info!("Generator Paused");
                self.operating_state = OperatingState::Paused;
            }
        }
    }

    fn generator_loop(&mut self) {
        // main mining loop
        let key = key_pair::random();
        let h:H256 = ring::digest::digest(&ring::digest::SHA256, key.public_key().as_ref()).into();
        let addr = H160::from(h);
        self.server.broadcast(Message::NewAddr(addr));
        self.table.lock().unwrap().push(addr);
        //Default: Create ICO
        // let g_trans: Transaction = Transaction { 
        //     input_previous:Vec::new(), 
        //     input_index:Vec::new(),
        //     output_address:vec![addr],
        //     output_value:vec![100 as u32], };
        // let g_signature = sign(&g_trans, &key);
        // let g_s = Sign{
        //     signature:g_signature.as_ref().to_vec().clone(),
        //     key:key.public_key().as_ref().to_vec(),
        // };
        // let g_out = SignedTransaction{
        //     transaction:g_trans.clone(),
        //     sig: g_s.clone(),
        // };
        let g_hash:HashMap<(H256,u32), (u32, H160)> = HashMap::new();
        self.glob_state.lock().unwrap().insert(self.blockchain.lock().unwrap().tip(),g_hash);
        // g_hash.insert( (H256::from([0;32]),0),(100,addr) );
        let trans: Transaction = Transaction { 
            input_previous:vec![H256::from([0;32])],
            input_index:vec![0 as u32],
            output_address:vec![addr],
            output_value:vec![100 as u32],};
        // println!("Generated transactions: {:?}",trans);
        let signature = sign(&trans, &key);
        let s = Sign{
            signature:signature.as_ref().to_vec().clone(),
            // key:temp.public_key().as_ref().to_vec(),
            key:key.public_key().as_ref().to_vec(),
        };
        let new_trans = SignedTransaction{
            transaction:trans.clone(),
            sig: s.clone(),
        };
        self.mempool.lock().unwrap().insert(new_trans.hash(), new_trans.clone());
        self.server.broadcast(Message::NewTransactionHashes(vec![new_trans.hash()]));
        let mut most_recent_state = H256::from([0;32]);
        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    self.handle_control_signal(signal,addr);
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        self.handle_control_signal(signal,addr);
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("generator control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                println!("shutting down");
                return;
            }
            if let OperatingState::Run(i) = self.operating_state {
                let tip = self.blockchain.lock().unwrap().tip();
                let interval = time::Duration::from_micros(1500000 as u64);
                    thread::sleep(interval);
                // println!("most recent {:?} tip {:?}",most_recent_state,tip);
                if most_recent_state == tip{
                    println!("stay here");
                    // self.operating_state = OperatingState::Paused;
                    continue;
                }
                // println!("new transactions");
                most_recent_state = tip;
                // println!("Generator curr view : {:?}",self.glob_state.lock().unwrap()[&tip]);
                for ((k,iter),(v,me)) in self.glob_state.lock().unwrap()[&tip].iter(){
                    let mut input_vec: Vec<H256> = Vec::new();
                    let mut idx_vec: Vec<u32> = Vec::new();
                    let mut val_vec: Vec<u32> = Vec::new();
                    let mut addr_vec: Vec<H160> = Vec::new();
                    let mut all = *v;
                    if *me != addr {
                        continue;
                    }
                    input_vec.push(*k);
                    idx_vec.push(*iter);
                    for _idx in 0..i{
                        let val = 1 + random::<u32>() % all;
                        let dest_idx:usize =(random::<u32>() as usize) % (self.table.lock().unwrap().len()) ;
                        val_vec.push(val);
                        addr_vec.push(self.table.lock().unwrap()[dest_idx]);
                        all = all - val;
                        if all == 0 {
                            break;
                        }
                    }
                    if all > 0{
                        val_vec.push(all);
                        let dest_idx:usize =(random::<u32>() as usize) % (self.table.lock().unwrap().len()) ;
                        addr_vec.push(self.table.lock().unwrap()[dest_idx]);
                    }
                    let trans: Transaction = Transaction { 
                        input_previous:input_vec.clone(),
                        input_index:idx_vec.clone(),
                        output_address:addr_vec.clone(),
                        output_value:val_vec.clone(),};
                    let signature = sign(&trans, &key);
                    let s = Sign{
                        signature:signature.as_ref().to_vec().clone(),
                        // key:temp.public_key().as_ref().to_vec(),
                        key:key.public_key().as_ref().to_vec(),
                    };
                    
                    let new_trans = SignedTransaction{
                        transaction:trans.clone(),
                        sig: s.clone(),
                    };
                    // debug!("Generated transactions: {:?}",trans);
                    self.mempool.lock().unwrap().insert(new_trans.hash(), new_trans.clone());
                    self.server.broadcast(Message::NewTransactionHashes(vec![new_trans.hash()]));
                }
                
                // for idx in 0..addr_vec.len(){
                //     debug!("{:?} to {:?}",val_vec[idx],addr_vec[idx]);
                // }
                // println!("Length {}",self.mempool.lock().unwrap().len());
                // println!("Mempool {:?}",self.mempool.lock().unwrap());
                // self.operating_state = OperatingState::Paused;
            }
        }
    }
}
