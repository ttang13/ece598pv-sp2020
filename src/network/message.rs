use serde::{Serialize, Deserialize};
use crate::block::{BlockP,BlockT};
use crate::crypto::hash::{H256,H160};
use crate::transaction::SignedTransaction;
#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Message {
    NewAddr(H160),
    BroadcastAddr(H160),
    UpdateAddr(Vec<H160>),
    NewTransactionHashes(Vec<H256>),
    GetTransactions(Vec<H256>),
    Transactions(Vec<SignedTransaction>),
    NewBlockHashes(Vec<H256>),
    GetBlocks(Vec<H256>),
    Blocks(Vec<BlockP>),
    NewTxBlockHashes(Vec<H256>),
    GetTxBlocks(Vec<H256>),
    TxBlocks(Vec<BlockT>),
    Ping(String),
    Pong(String),
}
