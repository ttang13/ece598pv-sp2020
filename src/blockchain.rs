use crate::block::{BlockP,BlockT,Header,ContentP,ContentT};
use crate::crypto::hash::{H256,Hashable};
use std::collections::HashMap;
// use crate::crypto::merkle::MerkleTree;
use crate::crypto::key_pair;
// use rand::random;
// use std::time::SystemTime;
// use ring::signature::Ed25519KeyPair;
use crate::transaction::generate_random_transaction;
pub struct Blockchain {
	pub blocks: HashMap<H256, BlockP>, // hash of blocks and blocks themselves
    pub heights: HashMap<H256, u32>, // hash of blocks and their heights
    
	pub longest: u32, // height of tip
	pub tip_node_hash: H256 // the tip node
}


impl Blockchain {
    /// Create a new blockchain, only containing the genesis block
    pub fn new() -> Self {
        let key = key_pair::random();
        let mut transaction = generate_random_transaction(&key);
        transaction.sig.signature = Vec::new();
        transaction.sig.key = Vec::new();
        let difficulty_val: [u8; 32] = [128, 255, 255, 255, 255, 255, 255, 255, 
            255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255,];
        let difficulty_val_m: [u8; 32] = [32, 255, 255, 255, 255, 255, 255, 255, 
            255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255,];

        let fake_root = H256::from([0;32]);
        let header = Header{
            parent : fake_root,
            nonce : 1,
            difficulty : <H256>::from(&difficulty_val),
            difficulty_m : <H256>::from(&difficulty_val_m),
            timestamp :  1,
            merkle_root_p : fake_root,
            merkle_root_t : fake_root,
        };
        let fake_b_c = ContentT{data: vec![transaction.clone()],};
        let fake_genesis_block : BlockT = BlockT{
            header : header,
            content : fake_b_c,
        };
        let fake_content = ContentP{data: vec![fake_genesis_block.hash()],};
        
        let genesis_block : BlockP = BlockP{
            header : header,
            content : fake_content,
        };
        // println!("block {:?}", genesis_block);
        let mut blocks_hash_map = HashMap::new();
        blocks_hash_map.insert(genesis_block.clone().hash(), genesis_block.clone());
    
        let mut height_hash_map = HashMap::new();
        height_hash_map.insert(genesis_block.clone().hash(), 0);
    
        return Blockchain{
            blocks: blocks_hash_map,
            heights: height_hash_map,
            longest: 0,
            tip_node_hash: genesis_block.clone().hash(),
        };    
    }

    /// Insert a block into blockchain
    pub fn insert(&mut self, block: &BlockP) {
        self.blocks.insert(block.clone().hash(), block.clone());
		let curr_height = self.heights[&block.clone().header.parent] + 1;
		self.heights.insert(block.clone().hash(), curr_height);
		if curr_height > self.longest{
			self.longest = curr_height;
			self.tip_node_hash = block.clone().hash();
		}
    }

    /// Get the last block's hash of the longest chain
    pub fn tip(&self) -> H256 {
        self.tip_node_hash
    }

    /// Get the last block's hash of the longest chain
    #[cfg(any(test, test_utilities))]
    pub fn all_blocks_in_longest_chain(&self) -> Vec<H256> {
        unimplemented!()
    }
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::block::test::generate_random_block;
    use crate::crypto::hash::Hashable;

    #[test]
    fn insert_one() {
        let mut blockchain = Blockchain::new();
        let genesis_hash = blockchain.tip();
        let block = generate_random_block(&genesis_hash);
        blockchain.insert(&block);
        assert_eq!(blockchain.tip(), block.hash());
    }
}
