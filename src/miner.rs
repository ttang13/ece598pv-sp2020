use crate::network::server::Handle as ServerHandle;
use crate::blockchain::Blockchain;
use crate::block::{BlockP,BlockT,Header,ContentP,ContentT};
use crate::crypto::hash::{H256,H160,Hashable};
use crate::transaction::SignedTransaction;
use crate::crypto::merkle::MerkleTree;
use crate::network::message::Message;
use std::collections::HashMap;

use log::info;

use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time;
use rand::random;
use std::sync::{Arc, Mutex};
use std::thread;

enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    StartFix(u64),
    Paused,
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    RunFix(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    mempool: Arc<Mutex<HashMap<H256 ,SignedTransaction>>>,
    // orphan_transaction: Arc<Mutex<HashMap<H256 ,bool>>>,
    glob_state: Arc<Mutex<HashMap<H256, HashMap<(H256,u32), (u32, H160)> >>>,
    txpool: Arc<Mutex<HashMap<H256 ,BlockT>>>,
    unseen: Arc<Mutex<HashMap<H256 ,bool>>>,
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool: &Arc<Mutex<HashMap<H256 ,SignedTransaction>>>,
    // orphan_transaction: &Arc<Mutex<HashMap<H256 ,bool>>>,
    glob_state: &Arc<Mutex<HashMap<H256, HashMap<(H256,u32), (u32, H160)> >>>,
    txpool: &Arc<Mutex<HashMap<H256 ,BlockT>>>,
    unseen: &Arc<Mutex<HashMap<H256 ,bool>>>,
) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        server: server.clone(),
        blockchain: Arc::clone(blockchain),
        mempool: Arc::clone(mempool),
        // // orphan_transaction: Arc::clone(orphan_transaction),
        glob_state: Arc::clone(glob_state),
        txpool: Arc::clone(txpool),
        unseen: Arc::clone(unseen),
    };
    
    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
    }
    pub fn pause(&self) {
        self.control_chan.send(ControlSignal::Paused).unwrap();
    }
    pub fn startfix(&self, lambda: u64) {
        self.control_chan.send(ControlSignal::StartFix(lambda)).unwrap();
    }
    pub fn start(&self, lambda: u64) {
        self.control_chan
            .send(ControlSignal::Start(lambda))
            .unwrap();
    }

}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("miner".to_string())
            .spawn(move || {
                self.miner_loop();
            })
            .unwrap();
        info!("Miner initialized into paused mode");
    }

    fn handle_control_signal(&mut self, signal: ControlSignal) {
        match signal {
            ControlSignal::Exit => {
                info!("Miner shutting down");
                self.operating_state = OperatingState::ShutDown;
            }
            ControlSignal::Paused => {
                info!("Miner shutting down");
                self.operating_state = OperatingState::Paused;
            }
            ControlSignal::StartFix(i) => {
                info!("Mine {} blocks", i);
                self.operating_state = OperatingState::RunFix(i);
            }
            ControlSignal::Start(i) => {
                info!("Miner starting in continuous mode with lambda {}", i);
                self.operating_state = OperatingState::Run(i);
            }
        }
    }

    fn miner_loop(&mut self) {
        // main mining loop
        let mut idx_p = 0;
        let mut idx_t = 0;
        let start_time : f64= time::SystemTime::now().duration_since(time::SystemTime::UNIX_EPOCH).unwrap().as_millis() as f64;
        let mut total_transactions = 0;
        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    self.handle_control_signal(signal);
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        self.handle_control_signal(signal);
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("Miner control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                // println!("shutting down{:?}", self.blockchain.lock().unwrap().all_blocks_in_longest_chain_random_name());
                println!("shutting down");
                return;
            }

            if let OperatingState::RunFix(i) = self.operating_state {
                if i == 0{
                    self.operating_state = OperatingState::Paused;
                    continue;
                }
                self.operating_state = OperatingState::RunFix(i-1);
            }
            //Fix. This mining scheme might be buggy. 
            let lim_t:usize = 5;
            let lim_p:usize = 5;
            let tip = self.blockchain.lock().unwrap().tip();
            let mut parent_pool:HashMap<H256 ,bool> = HashMap::new();
            let mut new_state:HashMap<(H256,u32), (u32, H160)> = self.glob_state.lock().unwrap()[&tip].clone();
            let mut to_add_trans: Vec<SignedTransaction> = Vec::new();
            let mut to_remove_trans: Vec<H256> = Vec::new();
            let mut to_add_trans_b: Vec<H256> = Vec::new();
            let mut to_remove_trans_b: Vec<H256> = Vec::new();
            let m = self.mempool.lock().unwrap();
            let mut iter = m.keys();
            let mut curr_examine = tip;
            //build all parent pointed tx blocks
            loop{
                let curr_block = self.blockchain.lock().unwrap().blocks[&curr_examine].clone();
                if curr_block.header.parent == H256::from([0;32]){
                    break;
                }
                curr_examine = curr_block.header.parent;
                for item in curr_block.content.data{
                    if !parent_pool.contains_key(&item){
                        parent_pool.insert(item.clone(),true);
                    }
                }
            }

            for _k in 0..lim_t{
                let remove_key:H256;
                match iter.next(){
                    Some(keys) => {
                        remove_key = keys.clone();
                    }
                    None => {
                        break;
                    }
                }
                // if self.orphan_transaction.lock().unwrap().contains_key(&remove_key){
                //     println!("Miner: Found orphan transaction, Ignore!");
                //     continue;
                // }
                //before taking the transaction from pool to block, check valid
                let mut valid = true;
                
                for j in 0..m[&remove_key].transaction.input_previous.len(){
                    if m[&remove_key].transaction.input_previous[0] == H256::from([0;32]){
                        break;
                    }
                    let lookup_key = (m[&remove_key].transaction.input_previous[j],m[&remove_key].transaction.input_index[j]);
                    if !new_state.contains_key(&lookup_key){
                        println!("Bad bad {:?}",lookup_key.clone());
                        valid = false; 
                        break;
                    }
                }
                //Confirm valid, create new state
                if valid{
                    to_add_trans.push(m[&remove_key].clone());
                    to_remove_trans.push(remove_key.clone());
                }
            }
            std::mem::drop(m);

            let tx_local = self.unseen.lock().unwrap();
            let tx_local_lookup = self.txpool.lock().unwrap();
            let mut tx_iter = tx_local.keys();
            let mut tx_seen:HashMap<H256 ,bool> = HashMap::new();
            let mut to_remove_parent: Vec<H256> = Vec::new();
            for _k in 0..lim_p{
                let remove_key:H256;
                
                match tx_iter.next(){
                    Some(keys) => {
                        remove_key = keys.clone();
                    }
                    None => {
                        break;
                    }
                }
                if parent_pool.contains_key(&remove_key){
                    println!("block already in parent tree");
                    to_remove_parent.push(remove_key.clone());
                    continue;
                }
                let curr_tx_b = tx_local_lookup[&remove_key].clone();
                let mut valid = true;
                for curr_tx_idx in 0..curr_tx_b.content.data.len(){
                    let curr_tx = curr_tx_b.content.data[curr_tx_idx].clone();

                    for j in 0..curr_tx.transaction.input_previous.len(){
                        if curr_tx.transaction.input_previous[0] == H256::from([0;32]){
                            break;
                        }
                        let lookup_key = (curr_tx.transaction.input_previous[j],curr_tx.transaction.input_index[j]);
                        if !new_state.contains_key(&lookup_key){
                            // println!("Bad bad {:?}",lookup_key.clone());
                            valid = false; 
                            break;
                        }
                    }

                    if !valid{
                        to_remove_parent.push(remove_key.clone());
                        break;
                    }
                    // println!("curr transaction {:?}",curr_tx);
                    if tx_seen.contains_key(&curr_tx.hash()){
                        println!("Seen!");
                        continue;
                    }
                    tx_seen.insert(curr_tx.hash(),true);

                    for j in 0..curr_tx.transaction.input_previous.len(){
                        if curr_tx.transaction.input_previous[j] == H256::from([0;32]){
                            continue;
                        }
                        let lookup_key = (curr_tx.transaction.input_previous[j],curr_tx.transaction.input_index[j]);
                        // println!("TO Remove:{:?}\n{:?}",lookup_key,new_state[&lookup_key]);
                        new_state.remove(&lookup_key);
                    }
                    for j in 0..curr_tx.transaction.output_address.len(){
                        // println!("TO Insert:{:?} {:?}",(curr_tx.hash(), j as u32),(curr_tx.output_value[j],curr_tx.output_address[j]));
                        // println!("{:?} {:?}",curr_tx,curr_tx.hash());
                        new_state.insert((curr_tx.hash(), j as u32),(curr_tx.transaction.output_value[j],curr_tx.transaction.output_address[j]));
                    }
                }
                if valid{
                    to_add_trans_b.push(remove_key.clone());
                    to_remove_trans_b.push(remove_key.clone());
                }
            }
            std::mem::drop(tx_local);
            std::mem::drop(tx_local_lookup);
            for item in to_remove_parent{
                self.unseen.lock().unwrap().remove(&item);
            }
            let new_parent = self.blockchain.lock().unwrap().tip();
            let new_difficulty = self.blockchain.lock().unwrap().blocks[&new_parent].header.difficulty;
            let new_difficulty_m = self.blockchain.lock().unwrap().blocks[&new_parent].header.difficulty_m;
            let new_merkle_root_t = MerkleTree::new(&to_add_trans).root();  
            let new_merkle_root_p = MerkleTree::new(&to_add_trans_b).root();  
            let new_content_p = ContentP{data: to_add_trans_b.clone()};
            let new_content_t = ContentT{data: to_add_trans.clone()};
            for _iterator in 0..20{
                let header = Header{
                    parent : new_parent,
                    nonce : random::<u32>(),
                    difficulty : new_difficulty,
                    difficulty_m : new_difficulty_m,
                    timestamp :  time::SystemTime::now().duration_since(time::SystemTime::UNIX_EPOCH).unwrap().as_millis(),
                    merkle_root_p : new_merkle_root_p,
                    merkle_root_t : new_merkle_root_t,
                };
                
                if header.hash() <= new_difficulty {
                    //Classify as proposer block
                    if header.hash() <= new_difficulty_m{
                        if new_content_p.data.len()==0{
                            println!("Proposer mined, but no transaction block");
                            break;
                        }
                        let new_block = BlockP{
                            header : header.clone(),
                            content : new_content_p.clone(),
                        };
                        self.blockchain.lock().unwrap().insert(&new_block);
                        self.glob_state.lock().unwrap().insert(new_block.hash(),new_state.clone());
                        self.server.broadcast(Message::NewBlockHashes(vec![new_block.hash()]));
                        for _k in 0..to_remove_trans_b.len(){
                            self.unseen.lock().unwrap().remove(&to_remove_trans_b[_k]);
                            total_transactions = total_transactions + self.txpool.lock().unwrap()[&to_remove_trans_b[_k]].content.data.len();
                        }
                        println!("proposer block! {:?} tx blocks{:?} longest chain {:?}",new_block.hash(),new_block.content.data.len(),self.blockchain.lock().unwrap().blocks.len());
                        let now: f64 = time::SystemTime::now().duration_since(time::SystemTime::UNIX_EPOCH).unwrap().as_millis() as f64;
                        println!("Total transactions: {:?} Rate {:?}", total_transactions,1000.0*(total_transactions as f64)/(now - start_time));
                        idx_p = idx_p + 1;
                        
                    }
                    //Classify as transaction block
                    else{
                        if new_content_t.data.len()==0{
                            println!("Transaction Block mined, but no transaction");
                            break;
                        }
                        let new_block = BlockT{
                            header : header.clone(),
                            content : new_content_t.clone(),
                        };
                        self.server.broadcast(Message::NewTxBlockHashes(vec![new_block.hash()]));
                        self.txpool.lock().unwrap().insert(new_block.hash(),new_block.clone());
                        self.unseen.lock().unwrap().insert(new_block.hash(),true);
                        for _k in 0..to_remove_trans.len(){
                            self.mempool.lock().unwrap().remove(&to_remove_trans[_k]);
                        }
                        println!("Transaction block! {:?} content len {:?}",new_block.hash(),new_content_t.data.len());
                        idx_t = idx_t+1;
                    }
                    println!("Total: {}",idx_p+idx_t);
                    break;
                }
            }
            if let OperatingState::Run(i) = self.operating_state {
                if i != 0 {
                    let interval = time::Duration::from_micros(i as u64);
                    thread::sleep(interval);
                }
            }
        }
    }
}
